# Characterization board

Here you will find the hardware files of the Characterization board housing an lpGBT ASIC.

![Characterization Board](Docs/img/Characterization_Board.png)